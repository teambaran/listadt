/****************************************************************************************
/**
*	Lex.c : Contains main for pa2. Reads strings from given file and outputs those strings
*			in alphabetical order to the other given file by using the List.c ADT.
*	Name: Kaile Baran
*	Student ID: 1573206
*	Assignment: Programming Assignment 2
*	Course: CMPS 101
*
*
*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"List.h"

int main(int argc, char * argv[])
{
	int MAX_LEN = 160;
	FILE *in, *out;
	int lineCount=0;
	char line[MAX_LEN];
	List L = newList();
	//Checking that the number of arguments is correct.
	if( argc != 3 )
	{
		printf("Invalid number of arguments");
		exit(1);
	}
	//Opening input and output files.
	in = fopen(argv[1], "r");
	out = fopen(argv[2], "w");
	//Checking if the files were able to be opened successfully.
	if(in==NULL)
	{
		printf("Unable to open file %s for reading\n", argv[1]);
		exit(1);
    }
	if(out==NULL)
	{
		printf("Unable to open file %s for writing\n", argv[2]);
		exit(1);
    }
	int i=0;
	//Counting lines of file
	while(!feof(in))
	{
		fscanf(in, "%s", line);
		lineCount++;
	}
	//Array to hold the data read in from the input file
	const char *lines[lineCount];
	
	rewind(in);
	int lineIndex=0;
	//Creating array of Strings from file
	while(!feof(in))
	{
		fscanf(in,"%s",line);
		lines[lineIndex]=strdup(line);
		lineIndex++;
	}
	fclose(in);
	//Placing the indexes of Strings into a List in alphabetical order.
	append(L,0);
	int added;
	int j;
	int cmp;
	for(j=1;j<lineCount;j++)
	{
		added = 0;
		moveFront(L);
		if(length(L)==1)
		{			
			cmp = strcmp(lines[j], lines[get(L)]);
			if(cmp > 0)
			{
				append(L,j);
			}
			else if(cmp < 0)
			{
				prepend(L, j);
			}
		}
		else
		{
			moveFront(L);
			cmp = strcmp(lines[j] , lines[get(L)]);
			if(cmp < 0)
			{
				prepend(L, j);
				added =1;
			}
			while(index(L)!=-1 && cmp > 0)
			{
				moveNext(L);
				cmp = strcmp(lines[j] , lines[get(L)]);
			}
			if(index(L)==-1)
			{
				append(L,j);
				added=1;
			}
			else if(index(L)!=-1 && added == 0)
			{
				insertBefore(L,j);
				added =1;
			}
		}
	}
	//Outputting Strings from read in array to the output file in the order specified via the List.
	moveFront(L);
	while(index(L) != -1)
	{
		fprintf(out, "%s\n" , lines[get(L)]);
		moveNext(L);
	}
	//Freeing memory and closing output file.
	clear(L);
	freeList(&L);
	fclose(out);
	
	return 0;
}

//gcc -std=c99 List.h -std=c99 List.c ListClient.c