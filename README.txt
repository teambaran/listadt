README file for PA2
Written by: Kaile Baran Student ID:1573206 Class: CMPS101


Lex.c : Contains a main for PA2 that has the goal of using List.c to alphabetize lines
	from a given input file, and sending those lines to a given output file. Will take
	two arguments from the command prompt to be used as the input and output files.

List.c: My implementation of a List ADT implemented as a double ended queue with a cursor
	element that can be undefined if it happens to "fall off" the List. The data objects
	used in List.c are Nodes.

List.h: Functions as the header file for List.c

Makefile.txt: Standard Makefile provided for the assignment. Creates an exe file called Lex and includes a
	      clean utility that will remove all .o files (Lex included).

README.txt: This file. Explains the purpose of all included files in pa2 folder.